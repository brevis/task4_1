package org.javastudy.task4_1.weather;

import java.util.ArrayList;
import java.util.Random;

public class Weather {

    public enum PrecipitationLevel {
        NONE, LOW, MEDIUM, HIGH;
    }

    private int precipitation; // mm per 12 hour

    public void setPrecipitation(int precipitation) {
        this.precipitation = precipitation;
    }

    public PrecipitationLevel getPrecipitationLevel() {
        if (precipitation == 0) {
            return PrecipitationLevel.NONE;
        } else if (precipitation > 0 && precipitation <= 2) {
            return PrecipitationLevel.LOW;
        } else if (precipitation > 2 && precipitation <= 14) {
            return PrecipitationLevel.MEDIUM;
        } else {
            return PrecipitationLevel.HIGH;
        }
    }

    public static ArrayList<Weather> getForecast(int days) {
        ArrayList<Weather> forecast = new ArrayList<Weather>();
        for (int i=0; i<days;i++) {
            Weather weather = new Weather();
            weather.setPrecipitation(new Random().nextInt(30));
            forecast.add(weather);
        }
        return forecast;
    }
}
