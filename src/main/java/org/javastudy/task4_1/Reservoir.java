package org.javastudy.task4_1;

import org.javastudy.task4_1.weather.Weather;
import java.util.HashMap;

public class Reservoir {
    double waterAmount;
    double minWaterAmount;
    double maxWaterAmount;
    double consumption;
    HashMap<Weather.PrecipitationLevel, Integer> consumptionLevels;

    public Reservoir(double waterAmount, double minWaterAmount, double maxWaterAmount, HashMap<Weather.PrecipitationLevel, Integer> consumptionLevels) {
        this.waterAmount = waterAmount;
        this.minWaterAmount = minWaterAmount;
        this.maxWaterAmount = maxWaterAmount;
        this.consumptionLevels = consumptionLevels;
    }

    public double getConsumption() {
        return consumption;
    }

    public double getConsumptionByLevel(Weather.PrecipitationLevel level) {
        if (consumptionLevels.containsKey(level)) {
            return (double)consumptionLevels.get(level);
        }
        return 0; // TODO: change to throw Exception
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public double getCurrentWaterAmount() {
        return waterAmount;
    }

    public double getFreeSpace() {
        return maxWaterAmount - waterAmount;
    }

    public void fill(double waterAmount) throws ReservoirFullException {
        double freeSpace = getFreeSpace();
        if (waterAmount > freeSpace) {
            waterAmount = freeSpace;
            this.waterAmount += waterAmount;
            throw new ReservoirFullException();
        } else {
            this.waterAmount += waterAmount;
        }
    }

    public double watering(double seconds) {
        if (waterAmount <= minWaterAmount) return 0;
        double amount = consumption / 3600 * seconds;
        if (amount > waterAmount - minWaterAmount) amount = waterAmount - minWaterAmount;
        this.waterAmount -= amount;
        return amount;
    }

}
