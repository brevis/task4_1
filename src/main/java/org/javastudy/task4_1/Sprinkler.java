package org.javastudy.task4_1;

import java.util.ArrayList;
import java.util.HashMap;

import org.javastudy.task4_1.weather.Weather;

public class Sprinkler {

    private Reservoir reservoir;
    private Well well;
    private Pump pump;
    private int workingDays = 0;
    private int workingHours = 0;
    private ArrayList<Weather> forecast;
    final private int iterationTime = 1; // seconds

    public void setReservoir(Reservoir reservoir) {
        this.reservoir = reservoir;
    }

    public void setWell(Well well) {
        this.well = well;
    }

    public void setPump(Pump pump) {
        this.pump = pump;
    }

    public void setWorkingDays(int workingDays) {
        this.workingDays = workingDays;
    }

    public void setWorkingHours(int hours) {
        this.workingHours = hours;
    }

    public void setForecast(ArrayList<Weather> forecast) {
        this.forecast = forecast;
    }

    public double calculateWaterDeficit(boolean debug) {
        double waterWatering = 0;
        double waterNeeded = 0;
        int i = 0;
        for (Weather weather : forecast) {
            waterNeeded += reservoir.getConsumptionByLevel(weather.getPrecipitationLevel()) * workingHours;
            i++;
            if (i >= forecast.size() - 1) break;
        }

        if (waterNeeded == 0) {
            System.out.println("No watering needed");
            return 0;
        }

        for (int currentDay = 0; currentDay < workingDays; currentDay++) {
            Weather.PrecipitationLevel precipitationLevel = forecast.get(currentDay).getPrecipitationLevel();
            double reservoirConsumption = reservoir.getConsumptionByLevel(precipitationLevel);
            reservoir.setConsumption(reservoirConsumption);

            if (debug) {
                System.out.println("--- DAY " + (currentDay + 1) + ": " + reservoirConsumption + " l/h ---");
            }

            int currentSecond = 0;
            int pumpOffTime = -(pump.getSleepTime() * 60 + 1);
            while (currentSecond < 3600 * workingHours) {
                // pump water (well -> reservoir)
                if (currentSecond - pumpOffTime > pump.getSleepTime() * 60) {
                    try {
                        pump.pump(iterationTime); // pump during `iterationTime` second(s)
                    } catch (ReservoirFullException e) {
                        pumpOffTime = currentSecond;
                    }
                }

                // watering (reservoir -> ...)
                if (precipitationLevel != Weather.PrecipitationLevel.HIGH) {
                    waterWatering += reservoir.watering(iterationTime);
                }

                // refill well (... -> well)
                well.refill(iterationTime); // fill well during `iterationTime` second(s)

                currentSecond += iterationTime;

                // debug
                if (debug && currentSecond % 3600 == 0) {
                    System.out.println(currentSecond / 3600 + " hour:");
                    System.out.println("Water in reservoir: " + reservoir.getCurrentWaterAmount());
                    System.out.println("Water in well: " + well.getCurrentWaterAmount());
                    System.out.println("Watering: " + waterWatering);
                    System.out.println();
                }
            }
        }

        return waterNeeded - waterWatering;
    }

    public static void main(String[] args) {
        double wellWaterAmount = 2800; // liter
        double wellFillSpeed = 300; // liter per hour
        Well well = new Well(wellWaterAmount, wellFillSpeed);

        HashMap<Weather.PrecipitationLevel, Integer> consumptionLevels = new HashMap<Weather.PrecipitationLevel, Integer>();
        consumptionLevels.put(Weather.PrecipitationLevel.NONE, 210);
        consumptionLevels.put(Weather.PrecipitationLevel.LOW, 45);
        consumptionLevels.put(Weather.PrecipitationLevel.MEDIUM, 30);
        consumptionLevels.put(Weather.PrecipitationLevel.HIGH, 0);

        double reservoirWaterAmount = 100; // liter
        double reservoirMinAmount = 100; // liter
        double reservoirMaxAmount = 900; // liter
        Reservoir reservoir = new Reservoir(reservoirWaterAmount, reservoirMinAmount, reservoirMaxAmount, consumptionLevels);

        double pumpPower = 5000; // liter per hour
        int pumpSleepTime = 30; // minutes
        Pump pump = new Pump(pumpPower, pumpSleepTime, well, reservoir);

        int workingDays = 7; // working day number
        ArrayList<Weather> forecast = Weather.getForecast(workingDays + 1); // forecast for next `days` days

        Sprinkler sprinkler = new Sprinkler();
        sprinkler.setPump(pump);
        sprinkler.setReservoir(reservoir);
        sprinkler.setWell(well);
        sprinkler.setForecast(forecast);
        sprinkler.setWorkingDays(workingDays);
        sprinkler.setWorkingHours(9);

        boolean debug = true;
        double waterDeficit = sprinkler.calculateWaterDeficit(debug);
        if (waterDeficit > 0) {
            System.out.printf("Water deficit: %.10f liter", waterDeficit);
        } else {
            System.out.printf("No water deficit");
        }
    }

}
